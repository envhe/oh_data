# Oh Data!

a generic function and overview of using odata to pull reference data from eg StatsWales API 

## Support
The information contained in this repo is not intended to be comprehensive, and builds on other publically available guidance and documentation such as https://statswales.gov.wales/Help/Catalogue#StatsWalesODataGuidance.

## Contributing
The examples in this repo are intended to be illustrative, there may well (and quite likley to) be 'better' methods available. Please feel free to make use of the code for your purpose. Any suggestions for improvements or expansion to the examples will be gratefully received.

## Authors and acknowledgment
This notebook was produced by Rowena Bailey, HDRUK, Swansea University. The information is intended to aid fellow anlaysts and researchers make better use of publically available data. There is no requirement for author acknowledgement in subsequent use. 


## Project status
This code is not being maintained or checked for current validity, please refer to StatsWales for any changes to the OData API which may affect the functionality of this method.